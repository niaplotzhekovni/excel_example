import fetch from 'node-fetch';
import mongoose from 'mongoose';
import ExcelJS from 'exceljs';

const apiUrl = 'https://secret/search/companies';
const itemsPerPage = 100;
const alphabet = 'abcdefghijklmnopqrstuvwxyz';

async function connectToMongoDB() {
    const uri='mongodb+srv://niapl:Vg@cluster0.fff83cc.mongodb.net/?retryWrites=true&w=majority'
    await mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    console.log('Connected to MongoDB');
    return mongoose.connection;
}

const uniqueCompanySchema = new mongoose.Schema({
    company_number: { type: String, unique: true, required: true },
    currentQuery: { type: String, required: true },
    review: {type: Boolean, required: true}
});

const UniqueCompanyModel = mongoose.model('UniqueCompany', uniqueCompanySchema);

const updatedItemSchemaСharges = new mongoose.Schema({
    company_number: { type: String, required: true },
    charges: { type: Object },
    details: { type: Object }
});

const UpdatedItemModelCharges = mongoose.model('updatedItemSchemaСharges', updatedItemSchemaСharges);

const updatedItemSchemaHistory = new mongoose.Schema({
    company_number: { type: String, required: true },
    history: { type: Object },
    details: { type: Object }
});

const UpdatedItemModelHistory = mongoose.model('updatedItemSchemaHistory', updatedItemSchemaHistory);


async function fetchData(query) {
    const urlWithParams = `${apiUrl}?q=${query}&items_per_page=${itemsPerPage}`;

    try {
        const response = await fetch(urlWithParams, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '2ce6faf7-cdcb-427676'
            },
        });

        if (!response.ok) {
            console.log(`Network response was not ok: ${response.statusText}`);
            await sleep(300000);
            main()
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(`Error fetching data for query ${query}:`, error.message);
        await sleep(300000);
        main()
    }
}

async function fetchCharges(companyNumber) {
    const chargesUrl = `https://secret/company/${companyNumber}/charges`;

    try {
        const response = await fetch(chargesUrl, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '2ce6faf7-cdcb-427667'
            },
        });

        if (!response.ok) {
            console.log(`Network response was not ok: ${response.statusText}`);
            await sleep(300000)
            main()
        }

        const chargesData = await response.json();
        return chargesData;
    } catch (error) {
        console.error(`Error fetching charges data for company ${companyNumber}:`, error.message);
        return null;
    }
}

async function fetchHistory(companyNumber) {
    const historyUrl = `https://secret/company/${companyNumber}/filing-history`;

    try {
        const response = await fetch(historyUrl, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '2ce6faf7-cdcb-427676'
            },
        });

        if (!response.ok) {
            console.log(`Network response was not ok: ${response.statusText}`);
            await sleep(300000)
            main()
        }

        const historyData = await response.json();
        return historyData;
    } catch (error) {
        console.error(`Error fetching history data for company ${companyNumber}:`, error.message);
        return null;
    }
}

async function fetchCompanyDetails(companyNumber) {
    const companyDetailsUrl = `https://secret/company/${companyNumber}`;

    try {
        const response = await fetch(companyDetailsUrl, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '2ce6faf7-cdcb-426565'
            },
        });

        if (!response.ok) {
            console.log(`Network response was not ok: ${response.statusText}`);
            await sleep(300000)
            main()
        }

        const companyDetails = await response.json();
        return companyDetails;
    } catch (error) {
        console.error(`Error fetching company details for company ${companyNumber}:`, error.message);
        return null;
    }
}

async function writeDataToExcelCharges(data) {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Company Data Charges');

    const headers = [
        'Company Name',
        'Company Number',
        'Registered Office Address',
        'Charges Etag',
        'Charges Charge_code',
        'Charges Classification Type',
        'Charges Classification Description',
        'Charges Charge_number',
        'Charges Status',
        'Charges Delivered_on',
        'Charges Created_on',
        'Charges Particulars Type',
        'Charges Particulars Description',
        'Charges Persons_Entitled',
        'Charges Transaction Filing_type',
        'Charges Transaction Delivered_on',
        'Charges Transaction Links',
    ];
    worksheet.addRow(headers);

    data.forEach(company => {
        const { details, charges } = company;

        if (charges) {
            const classificationType = charges.classification ? charges.classification.type : '';
            const classificationDescription = charges.classification ? charges.classification.description : '';

            const particularsType = charges.particulars ? charges.particulars.type : '';
            const particularsDescription = charges.particulars ? charges.particulars.description : '';

            const personsEntitled = charges.persons_entitled ? charges.persons_entitled.map(person => person.name).join(', ') : '';

            const mainRowData = [
                details.company_name,
                details.company_number,
                details.registered_office_address ? details.registered_office_address.address_line_1 : '',
                charges.etag || '',
                charges.charge_code || '',
                classificationType,
                classificationDescription,
                charges.charge_number || '',
                charges.status || '',
                charges.delivered_on || '',
                charges.created_on || '',
                particularsType,
                particularsDescription,
                personsEntitled,
            ];

            worksheet.addRow(mainRowData);

            charges.transactions.forEach(transaction => {
                const transactionRowData = [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    transaction.filing_type || '',
                    transaction.delivered_on || '',
                    transaction.links?.filing || '',
                ];

                worksheet.addRow(transactionRowData);
            });
        }
    });

    await workbook.xlsx.writeFile('CompanyDataCharges.xlsx');
}

async function writeDataToExcelHistory(data) {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Company Data History');

    const headers = [
        'Company Name',
        'Company Number',
        'Registered Office Address',
        'History Document Metadata',
        'History Date',
        'History Type',
        'History Paper Filed',
        'History Description',
        'History Category',
        'History Pages',
        'History Barcode',
        'History Transaction ID',
    ];
    worksheet.addRow(headers);

    data.forEach(company => {
        const { details, history } = company;

        if (history) {
            const rowData = [
                details.company_name,
                details.company_number,
                details.registered_office_address ? details.registered_office_address.address_line_1 : '',
                history.document_metadata || '',
                history.date || '',
                history.type || '',
                history.paper_filed || '',
                history.description || '',
                history.category || '',
                history.pages || '',
                history.barcode || '',
                history.transaction_id || '',
            ];

            worksheet.addRow(rowData);
        }
    });

    await workbook.xlsx.writeFile('CompanyDataHistory.xlsx');
}

async function getCurrentQueryFromCollection() {
    const latestCompany = await UniqueCompanyModel.findOne().sort({ _id: -1 });

    if (latestCompany && latestCompany.currentQuery) {
        return latestCompany.currentQuery;
    } else {
        return 'a';
    }
}
async function iterateAlphabet() {
    let currentQuery = await getCurrentQueryFromCollection();

    while (currentQuery) {
        console.log(currentQuery)
        const count = await UniqueCompanyModel.estimatedDocumentCount();
        console.log(`Count: ${count}`);
        const data = await fetchData(currentQuery);
        if (data.total_results > 100) {
            console.log(`Query: ${currentQuery}, Total Results: ${data.total_results}`);
            currentQuery += 'a';
        } else {
            console.log(`Query: ${currentQuery}, Total Results: ${data.total_results}`);

            if (data.items && data.items.length > 0) {
                for (const item of data.items) {
                    if (item.company_number) {
                        await UniqueCompanyModel.updateOne(
                            { company_number: item.company_number },
                            {
                                $setOnInsert: {
                                    company_number: item.company_number,
                                    currentQuery: currentQuery,
                                    review: false
                                }
                            },
                            { upsert: true }
                        );
                    }
                }
            }

            if (currentQuery.split('').every(char => char === 'z')) {
                break;
            } else if (currentQuery[currentQuery.length - 1] === 'z') {
                const updateString = currentQuery.slice(0, -1);
                await sleep(1000);
                const nextQuery = getNextQuery(updateString);
                if (nextQuery !== currentQuery) {
                    currentQuery = nextQuery;
                } else {
                    break;
                }
            } else {
                await sleep(1000);
                const nextQuery = getNextQuery(currentQuery);
                if (nextQuery !== currentQuery) {
                    currentQuery = nextQuery;
                } else {
                    break;
                }
            }
        }
    }

    console.log('Unique Company Numbers have been saved to MongoDB');

    const uniqueCompanyNumbers = await UniqueCompanyModel.find({ review: false });
    for (const company of uniqueCompanyNumbers) {
        await sleep(500);
        const chargesData = await fetchCharges(company.company_number);
        if (chargesData && chargesData.items && chargesData.items.length > 0) {
            const today = new Date();
            let closestCharge = chargesData.items[0];
            let closestDateDifference = Math.abs(today - new Date(chargesData.items[0].created_on));
            for (let i = 1; i < chargesData.items.length; i++) {
                const currentCharge = chargesData.items[i];
                const currentDateDifference = Math.abs(today - new Date(currentCharge.created_on));

                if (currentDateDifference < closestDateDifference) {
                    closestCharge = currentCharge;
                    closestDateDifference = currentDateDifference;
                }
            }
            const companyDetails = await fetchCompanyDetails(company.company_number);
            if (companyDetails) {
                console.log(closestCharge)
                await UpdatedItemModelCharges.create({
                    company_number: company.company_number,
                    charges: closestCharge,
                    details: companyDetails
                });
            }
        }
        await sleep(500)
        const historyData = await fetchHistory(company.company_number);
        if (historyData && historyData.items && historyData.items.length > 0) {
            let selectedRecords = [];
            for (const item of historyData.items) {
                if (
                    item.description === 'accounts-with-accounts-type-medium' ||
                    item.description === 'accounts-with-accounts-type-medium-group' ||
                    item.description === 'accounts-with-accounts-type-small' ||
                    item.description === 'accounts-with-accounts-type-small-group'
                ) {
                    selectedRecords.push(item);
                }
            }
            let closestRecord = null;

            if (selectedRecords.length > 0) {
                const today = new Date();
                closestRecord = selectedRecords.reduce((closest, current) => {
                    const currentDate = new Date(current.date);
                    const closestDate = closest ? new Date(closest.date) : null;

                    if (!closestDate || Math.abs(today - currentDate) < Math.abs(today - closestDate)) {
                        return current;
                    } else {
                        return closest;
                    }
                });
            }
            if (closestRecord) {
                const today = new Date();
                const historyDate = new Date(closestRecord.date);
                const monthsDifference = (today.getFullYear() - historyDate.getFullYear()) * 12 + today.getMonth() - historyDate.getMonth();
                if (monthsDifference > 18 && monthsDifference < 24) {
                    const companyDetails = await fetchCompanyDetails(company.company_number);
                    if (companyDetails) {
                        await UpdatedItemModelHistory.create({
                            company_number: company.company_number,
                            history: closestRecord,
                            details: companyDetails
                        });
                    }
                }
            }
        }
        await UniqueCompanyModel.updateOne(
            { _id: company._id },
            { $set: { review: true } }
        );
    }
    const dataBaseCharges = await UpdatedItemModelCharges.find()
    writeDataToExcelCharges(dataBaseCharges);
    const dataBaseHistory = await UpdatedItemModelHistory.find()
    console.log('Whole Base History:', JSON.stringify(dataBaseHistory, null, 2));
    writeDataToExcelHistory(dataBaseHistory);
}

function getNextQuery(currentQuery) {
    while (currentQuery.endsWith('z')) {
        currentQuery = currentQuery.slice(0, -1);
    }
    return currentQuery.slice(0, -1) + getNextLetter(currentQuery.slice(-1));
}

function getNextLetter(letter) {
    return alphabet[(alphabet.indexOf(letter) + 1) % alphabet.length];
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {
    await connectToMongoDB();
    await iterateAlphabet();
    mongoose.disconnect();
}
main();
